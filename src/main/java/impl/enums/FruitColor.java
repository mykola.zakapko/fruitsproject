package impl.enums;

public enum FruitColor {

    YELLOW("Yellow"),
    GREEN("Green"),
    RED("Red");

    FruitColor(String color) {
        this.color = color;
    }

    private String color;

    public String getColor() {
        return color;
    }
}
