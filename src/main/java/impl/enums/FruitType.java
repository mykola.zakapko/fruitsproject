package impl.enums;

public enum FruitType {

    LEMON("Lemon"),
    APPLE("Apple"),
    BANANA("Banana");

    private String type;

    FruitType(String name) {
        this.type = name;
    }

    public String getType() {
        return type;
    }
}


