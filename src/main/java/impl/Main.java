package impl;

import impl.core.Printer;
import impl.core.Receipt;
import impl.core.Shop;
import impl.enums.FruitColor;
import impl.enums.FruitType;
import impl.models.Fruit;

import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        final ArrayList<Fruit> shopList = createShopList();
        final Receipt receipt;
        final Shop shop = new Shop();
        final Printer printer = new Printer();
        shop.addProductToCart(shopList);
        printer.printReceipt(shop.getReceipt());
    }


    public static ArrayList<Fruit> createShopList() { // создаем фрукты
        Fruit lemon = new Fruit(FruitType.LEMON, FruitColor.YELLOW, 50, 49.99);
        Fruit appleGreen = new Fruit(FruitType.APPLE, FruitColor.GREEN, 200, 80.25);
        Fruit banana = new Fruit(FruitType.BANANA, FruitColor.YELLOW, 450, 20.99);
        Fruit appleRed = new Fruit(FruitType.APPLE, FruitColor.RED, 550, 30.80);
        Fruit appleRed2 = new Fruit(FruitType.APPLE, FruitColor.RED, 560, 30.80);
        return new ArrayList<>(Arrays.asList(lemon, appleGreen, banana, appleRed, appleRed2));
    }
}
