package impl.models;

import impl.enums.FruitColor;
import impl.enums.FruitType;

import java.util.Objects;

public class Fruit {
    private FruitType type ;
    private FruitColor color;
    private int weight;
    private double price;

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        Fruit fruit = (Fruit) object;
        return weight == fruit.weight &&
                Double.compare(fruit.price, price) == 0 &&
                type == fruit.type &&
                color == fruit.color;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, color, weight, price);
    }

    public Fruit(FruitType type, FruitColor color, int weight, double price) {
        this.type = type;
        this.color = color;
        this.weight = weight;
        this.price = price;
    }


    public FruitType getType() {
        return type;
    }

    public FruitColor getColor(){
        return color;
    }

    public int getWeight(){
        return weight;
    }

    public double getPrice(){
        return price;
    }
}
