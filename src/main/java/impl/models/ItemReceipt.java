package impl.models;

import impl.enums.FruitColor;
import impl.enums.FruitType;

import java.util.Objects;

public class ItemReceipt {
    private FruitType fruitType; // На эквивалентность проверяем только это поле, потому как оно определяет уникальность
    private FruitColor fruitColor; // На эквивалентность проверяем только это поле, потому как оно определяет уникальность
    private int weight;
    private  double cost;
    private int count;

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || getClass() != object.getClass()) return false;
        ItemReceipt that = (ItemReceipt) object;
        return fruitType == that.fruitType &&
                fruitColor == that.fruitColor;
    }

    @Override
    public int hashCode() {
        return Objects.hash(fruitType, fruitColor);
    }

    public ItemReceipt(FruitType fruitType, FruitColor fruitColor, int weight, double cost, int count) {
        this.fruitType = fruitType;
        this.fruitColor = fruitColor;
        this.weight = weight;
        this.cost = cost;
        this.count = count;
    }

    public FruitType getFruitType() {
        return fruitType;
    }

    public FruitColor getFruitColor() {
        return fruitColor;
    }

    public int getWeight() {
        return weight;
    }

    public double getCost() {
        return cost;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return fruitType.getType() +
                " " + fruitColor.getColor() +
                " " + weight + " gram" +
                " x" + count +
                " = " + cost + " UAH";
    }
}
