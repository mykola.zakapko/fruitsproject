package impl.core;

import impl.models.ItemReceipt;

import java.util.Set;

public class Receipt {
   private Set<ItemReceipt> receiptItemList;
    private double totalCost;

    public Receipt(Set<ItemReceipt> receiptItemList, double totalCost) {
        this.receiptItemList = receiptItemList;
        this.totalCost = totalCost;
    }

    public Set<ItemReceipt> getReceiptItemList() {
        return receiptItemList;
    }

    public double getTotalCost() {
        return totalCost;
    }

}
