package impl.core;

import impl.models.Fruit;
import impl.models.ItemReceipt;

import java.util.*;

public class Shop {

    private ArrayList<Fruit> cart = new ArrayList<>();


    public void addProductToCart(ArrayList<Fruit> fruits) {
        cart.addAll(fruits);
    }

    public Receipt getReceipt() {
        HashSet<ItemReceipt> items = new HashSet<>();
        int totalCount;
        double totalSum;
        int totalWeight;
        for (int i = 0; i < cart.size(); i++) {
            totalCount = 0;
            totalSum = 0;
            totalWeight = 0;
            for (int j = i; j < cart.size(); j++) {
                if (cart.get(i).getType().equals(cart.get(j).getType()) &&
                        cart.get(i).getColor().equals(cart.get(j).getColor())) {
                    totalCount++;
                    totalSum += getPricePerFruit(cart.get(j));
                    totalWeight += cart.get(j).getWeight();
                }
            }

            ItemReceipt itemReceipt = new ItemReceipt(cart.get(i).getType(), cart.get(i).getColor(),
                    totalWeight, roundOff(totalSum), totalCount);
            items.add(itemReceipt); // Использую hashSet чтобы не было дубликатов, и при добавлении на эквивалетность проверим только тип и цвет продукта
            // смотри equals and hashCode в классе ItemReceipt
        }
        return new Receipt(items, getTotalPrice(items));
    }

    private double getPricePerFruit(Fruit fruit) {
        return fruit.getWeight() / 1000.00 * fruit.getPrice();                           // Считаем стоимость конкретного фрукта учитывая его вес
    }

    private double roundOff(double value) {
        return Math.round(value * 100.00) / 100.00;
    }

    private double getTotalPrice(Set<ItemReceipt> itemReceipts) {
        double sum;
        sum = itemReceipts.stream().map(ItemReceipt::getCost).reduce(0d, Double::sum);
        return roundOff(sum);
    }
}

