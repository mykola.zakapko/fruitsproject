package impl.core;

import impl.models.ItemReceipt;

public class Printer {

    public  void printReceipt(Receipt receipt){
        System.out.println(" ################  Receipt  ############## \n");
        for (ItemReceipt itemReceipt : receipt.getReceiptItemList()){
            System.out.println(" " + itemReceipt);
        }
        System.out.println("\n Total Amount : " + receipt.getTotalCost() + " UAH");
        System.out.println("\n #############  Receipt End  #############");
    }

}
